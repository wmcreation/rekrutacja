
function menuActiveClass(el) {
  setTimeout(function(){
    if(el.activeIndex > 0){
      addClass(document.querySelector(".navbar-nav"), 'active');
    }else{
      removeClass(document.querySelector(".navbar-nav"), 'active');
    }
  }, 0);
}

function swiperPaginationColor(el) {
  setTimeout(function(){
    if(el.activeIndex == 0){
      addClass(el.pagination.$el[0],'firstSection')
    }else{
      removeClass(el.pagination.$el[0],'firstSection')
    }
  }, 0);
}

function swiperSectionAnimation(el) {
  var items = el.slides[el.activeIndex].querySelectorAll('.os-animation');
  if(items.length > 0){
    for (var i = 0; i < items.length; i++){
      var osElement = items[i];
      var osAnimationClass = osElement.getAttribute('data-os-animation');
      var osAnimationDelay = osElement.getAttribute('data-os-animation-delay');

      osElement.style.cssText = "animation-delay:" + osAnimationDelay;
      osElement.classList.add('animated');
      osElement.classList.add(osAnimationClass);
    }
  }
}

var homeHeaderSlider = new Swiper('.home_header__slider', {
  speed: 800,
  loop: true,
  navigation: {
    nextEl: '.home_header__nav .next',
    prevEl: '.home_header__nav .prev',
  },
});


var settings = {
  direction: 'vertical',
  touchReleaseOnEdges: true,
  autoHeight: true,
  slidesPerView: 'auto',
  hashNavigation: true,
  simulateTouch: false,
  mousewheel: {
    eventsTarged: '.full_section_wrapper',
    releaseOnEdges: true
  },
  speed: 800,
  pagination: {
    el: '.full_section_wrapper .swiper-pagination',
    clickable: true,
  },
  on: {
    init: function () {
      menuActiveClass(this);
      swiperPaginationColor(this);
      swiperSectionAnimation(this);
    },
    slideChange: function () {
      menuActiveClass(this);
      swiperPaginationColor(this);
      swiperSectionAnimation(this);
    },
  }
};

var fullSectionWrapper = new Swiper('.full_section_wrapper', settings);

function controllFullPageScroll() {
  if(window.innerWidth <= 768){
    if(fullSectionWrapper.initialized){
      fullSectionWrapper.destroy();
    }
  }else{
    if(fullSectionWrapper.initialized != true){
      fullSectionWrapper = new Swiper('.full_section_wrapper', settings);
    }
  }
}


var nav = document.querySelector(".navbar-nav .nav");
var menuButton = document.querySelector(".navbar-hamburger");
menuButton.addEventListener("click",function(e){
  toggleClass(this, 'active');
  toggleClass(nav, 'active');
},false);


function initializeFooterMap() {
  var map;

  var styles=[{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}];

  var icon = {
    url: "/img/map_pin.png", // url
    scaledSize: new google.maps.Size(36, 50), // scaled size
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(36/2, 50) // anchor
  };

  var myLatlng = new google.maps.LatLng(52.416280,16.929050);
  var mapOptions = {
    center: myLatlng, // Set our point as the centre location
    zoom: 15, // Set the zoom level
    mapTypeId: 'roadmap', // set the default map type
    disableDefaultUI: true,
    styles: styles
  };

  // Display a map on the page
  map = new google.maps.Map(document.querySelector(".footer_item__map"), mapOptions);
  // Allow our satellite view have a tilted display (This only works for certain locations)
  map.setTilt(45);

  // Add a marker to the map based on our coordinates
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    icon: icon
  });
}

initializeFooterMap();


Vue.use(VeeValidate);

new Vue({
  el: '#contact_form',
  data: {
    form: {
      submited: false,
      name: '',
      secondName: '',
      email: '',
      message: '',
      rodo: false
    }
  },
  methods: {
    submitContactForm () {
      this.form.submitted = true
      event.preventDefault()
      this.$validator.validateAll().then((result) => {
        if(result){

        }else{
          console.log('Correct them errors!')
        }
    })
    }
  },
  mounted: function () {

  }
})


window.addEventListener('DOMContentLoaded', function (event) {
  controllFullPageScroll();
}, false);


window.addEventListener('resize', function (event) {
  controllFullPageScroll();
}, false);


document.querySelector('.play_btn').onclick = () => {
  const instance = basicLightbox.create(`
		<iframe width="960" height="540" src="http://brick.development.hosting.davision.uk/wp-content/themes/brick/img/home-video.mp4" frameborder="0" allowfullscreen></iframe>
	`).show()
}