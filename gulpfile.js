var gulp  = require('gulp'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  cleanCss = require('gulp-clean-css'),
  concat = require( 'gulp-concat' ),
  uglify = require( 'gulp-uglify' ),
  babel = require('gulp-babel'),
  rename = require('gulp-rename'),
  postcss      = require('gulp-postcss'),
  autoprefixer = require('autoprefixer');

gulp.task('build-theme', function() {
  return gulp.src(['scss/*.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([ autoprefixer({ browsers: [
      'Chrome >= 35',
      'Firefox >= 38',
      'Edge >= 12',
      'Explorer >= 10',
      'iOS >= 8',
      'Safari >= 8',
      'Android 2.3',
      'Android >= 4',
      'Opera >= 12']})]))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('css/'))
    .pipe(cleanCss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css/'))
});

// gulp scripts.
// Uglifies and concat all JS files into one
gulp.task( 'scripts', function() {
  var scripts = [
    'js/vue.min.js',
    'js/vee-validate.js',
    'js/swiper.min.js',
    'js/helpFunction.js',
    'js/basicLightbox.min.js',
    //CUSTOM JS
    'js/custom.js',
  ];
  gulp.src( scripts )
      .pipe( concat( 'theme.min.js' ) )
      .pipe(babel({
        presets: ['es2015']
      }))
      .pipe( uglify() )
      .pipe( gulp.dest( 'js/' ) );

  gulp.src( scripts )
      .pipe( concat( 'theme.js' ) )
      .pipe( gulp.dest( 'js/' ) );

});

gulp.task('watch', ['build-theme'] ['scripts'], function() {
  gulp.watch(['scss/*.scss', 'scss/*/*.scss'], ['build-theme']);
  gulp.watch(['js/*.js'], ['scripts']);
});

gulp.task('default', ['build-theme'], function() {
});



